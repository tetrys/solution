Python version used 2.7.10

The main data structure is a dictionary with key the word as unicode string and values objects of a class containing the word info
There is also a word unicode string with the maximum occurences

The word info class is asembled from:

-the total occurences of the word
-the next word with the most occurences
-a list of tuples with elements -> name of the book containing the word, occurences of the word in book
-a flag indicating the list as sorted or not

Another approach would be to have to data structures:
A similar dictionary but without the next word and a sorted list of tuples word, word occurences.

The Book class keeps all the information for each book and reads the book with the appropriate encoding.

The time complexity for a first search of a word is O(B*log(B)) where B the number of the books containing the word.
The next time the time complexity is O(1) since the list doesn't need to be sorted again.
Sorting of the lists book,occurence for every word can also be done during initiation.
Then there is no need for the flag but initiation time will be longer.

Time complexity for common Nth word is O(N) the time needed to create the result list.

File reading is a major bottle neck.
Time complexity seems to be linear with the number of the books,total words as tested by making copies of the books
Avoiding regex can reduce the time significally

Saving the data structure to a binary file and loading it the next time the server is up reduces also the initial time by a significant factor measured around 6