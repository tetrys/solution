# -*- coding: utf-8 -*-
import tarfile
import os
import sys
import time
from threading import Thread
from book import Book
from word_info import WordInfo
import cPickle
import codecs

#List of Book Objects
bookList = []
#Dictionary with key=word (unicode string), value=object of class word_info
wordList = {}
#the word with the most occurences
mostCommonWord = ""

#Thread for data structure initialization
inited = False
initializing = False
initThread = Thread()

#Initialize with a thread to let server respond
def initialize(tarPath=""):
    global inited
    global initializing
    global initThread
    
    if(os.path.isfile(tarPath)):
        if not inited and not initializing:
            initThread = Thread(target=initializeFunction,args=(tarPath,))
            initThread.start()
            return 1
        elif initializing:
            return 2
        else:
            return 0
    else:
        return 3

def joinInitThread():
    global initThread
    initThread.join()

def initializeFunction(tarPath):
    print "Starting Data Initiation"
    global inited
    global initializing
    global booksPath

    initializing = True

    dbBooksPath = os.path.dirname(os.path.realpath(tarPath))

    booksPath=os.path.join(os.path.dirname(os.path.realpath(tarPath)),"books")

    print "Initing in "+booksPath

    #If there are saved binary files of the data structures load them
    loadRes = loadBooks(dbBooksPath)

    #Else extract tar file and construct data structure
    if loadRes!=0:
        extractBooks(tarPath)
        readBookList(booksPath)
        readBooks()

    initializing = False
    inited = True

    print "Data Initiation Completed"

#Extract the Tar File to books/ folder
def extractBooks(tarPath):
    if(os.path.isfile(tarPath)):
        outPath=os.path.dirname(os.path.realpath(tarPath))
        
        #skip untar if folder books exists
        if not os.path.isdir(os.path.join(outPath,"books")):
            
            try:
                tar = tarfile.open(tarPath)
                tar.extractall(outPath)
                tar.close()
                return 0
            except:
                #print "Cannot Extract Dir"
                return -2
        else:
            #print "Not a Dir "+outPath
            return 1
    else:
        return -1

#Populate the bookList with the books
def readBookList(booksPath):
    global bookList
    bookList = []
    if os.path.isdir(booksPath):
        for bookFile in os.listdir(booksPath):
            if bookFile.endswith(".txt"):
                bookList.append(Book(os.path.join(booksPath,bookFile),bookFile))
        return True
    else:
        return False

#Read the books and construct the data structure
def readBooks():
    global bookList
    global wordList
    global mostCommonWord
    
    #Read Book Objects and add the words to the local word,total number of occurancies dictionary
    for book in bookList:
        book.readBook()
        for k,v in book.wordDict.iteritems():
            if k in wordList:
                wordList[k].bookList.append((book.fileName,v))
                wordList[k].occurences+=v
            else:
                wordList[k] = WordInfo(v,[(book.fileName,v)])

    #Sort lists by occurences descending
    #This part can be done during search
    #COMMENTED OUT
    #for k in wordList:
    #    wordList[k].bookList.sort(key=lambda tup: tup[1],reverse=True)

    #Create am inverse sorted list of words by total number of occurancies
    if len(wordList)>0:
        sortedWordList = sorted(wordList,key=lambda k:wordList[k].occurences,reverse=True)
        mostCommonWord = sortedWordList[0]

        #For each word in wordList add the next word with the most occurences
        for i in xrange(0,len(wordList)-1):
            wordList[sortedWordList[i]].nextWord = sortedWordList[i+1]

    bookList = []

#Serialize data structures
def saveBooks(dbPath):
    global wordList
    global mostCommonWord

    if os.path.isdir(dbPath):
        try:
            fhand = open(os.path.join(dbPath,"wordList.bdb"),"wb+")
            cPickle.dump(wordList,fhand)
            fhand.close()
        except:
            pass
        
        try:
            fhand = open(os.path.join(dbPath,"mostCommonWord.bdb"),"wb+")
            cPickle.dump(mostCommonWord,fhand)
            fhand.close()
        except:
            pass

#Load data structure from binary file
def loadBooks(dbPath):
    result = 0
    
    global wordList
    global mostCommonWord

    wordListPath = os.path.join(dbPath,"wordList.bdb")
    mostCommonWordPath = os.path.join(dbPath,"mostCommonWord.bdb")

    if os.path.isfile(wordListPath):
        try:
            fhand = open(wordListPath,"rb")
            wordList = cPickle.load(fhand)
            fhand.close()
        except:
            result = result | 1
    else:
        result = result | 1

    if os.path.isfile(mostCommonWordPath):
        try:
            fhand = open(mostCommonWordPath,"rb")
            mostCommonWord = cPickle.load(fhand)
            fhand.close()
        except:
            result = result | 1
    else:
        result = result | 1

    return result

#Search for a word
def findWord(word):
    global wordList
    
    searchResult = []
    result = []
    
    #Assuming the byte string represents a UTF-8 encoded string
    word = word.decode("utf-8")
    word = word.lower()

    #Time complexity O(1) when sorted or O(b*log(b)) where b the number of books that contain the word 
    if word in wordList:
        #Sort the books and apply the flag
        #Next time searched no need to sort it
        if not wordList[word].bookListSorted:
            wordList[word].bookList.sort(key=lambda tup: tup[1],reverse=True)
            wordList[word].bookListSorted = True

        searchResult = wordList[word].bookList

        #Time complexity O(w where w: the number of books containing the word
        #Worst case w=n when all the books contain the word. Complexity = O(n)
        for elem in searchResult:
            #Send the result as a list of strings
            #Create the byte string using again UTF-8 encoding
            result.append(elem[0].encode("utf-8")+" "+str(elem[1]))
    else:
        result = ["Word not found!"]
    
    return result

def printCommon(commonNumStr):
    global mostCommonWord

    try:
        commonNum = int(commonNumStr)
        result = []
        if commonNum>len(wordList):
            commonNum = len(wordList)

        #Create the list
        i=0
        word = mostCommonWord
        while i<commonNum:
            #Send the result as a list of strings
            #Create the byte string using again UTF-8 encoding
            result.append(word.encode("utf-8")+" "+str(wordList[word].occurences))
            i+=1
            word = wordList[word].nextWord
    
        return result
    except Exception as xcpt:
        print(xcpt)
        return None

#For testing
def printBookList():
    for book in bookList:
        print book.filePath