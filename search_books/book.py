# -*- coding: UTF-8 -*-
import string
import platform
import re
import codecs
import unicodedata
import sys
#Class Book fields:
#wordDict: dictionary of key=word (unicode string) value=number of occurences in book (int)
#fileName: the Name of the book. It is assumed that all book files are contained in folder books/
#filePath: absolut file path to book file
#bookEncoding: the character encoding of the text, ASCII, ISO-8859-1, UTF-8

#Regex to distinguish pure words supporting Latin alphabet plus German characters umlaut and eszett
wordRegex = unicode("^["+string.punctuation+"]*[a-zA-ZüöäÜÖÄß]+["+string.punctuation+"]*$",encoding="utf-8",errors="replace")
#Dictionary for punctuations in utf-8
transTable = dict.fromkeys(i for i in xrange(sys.maxunicode) if unicodedata.category(unichr(i)).startswith('P'))

class Book:
    
    def __init__(self,bookPath,bookName):
        self.fileName = bookName
        self.wordDict = {}
        self.filePath = bookPath
        #print "Initing Book "+self.filePath
        self.bookEncoding = "ascii"

        #Find encoding of file according to file name
        #With -0 is UTF-8
        #With -8 is ISO-8859-1
        #Else is ASCII
        #This assumption may be wrong
        idx = self.fileName.rfind("-")
        if idx>0:
            codecStr = self.fileName[idx+1:-4]
            if codecStr=="0":
                self.bookEncoding = "utf-8"
            elif codecStr=="8":
                self.bookEncoding = "iso-8859-1"

    #Read the book and populate wordDict
    #Time complexity O(N) where N: number of words in book
    def readBook(self):
        try:
            #Open with the encoding
            with codecs.open(self.filePath,"r",encoding=self.bookEncoding,buffering=1) as fhand:
                for line in fhand:
                    #There might be some errors in books and words are not
                    #seperated properly with space following a punctuation.
                    #For example: and then,he ...
                    #then,he will not be considered a word
                    #we could use a regex split here
                    wordList = line.split()
                    for word in wordList:
                        #Filter the words
                        if re.match(wordRegex,word):
                            #Remove punctuation
                            word = word.translate(transTable)
                            if word:
                                word = word.lower()
                                if word not in self.wordDict:
                                    self.wordDict[word] = 1
                                else:
                                    self.wordDict[word] += 1 
        except Exception as xcpt:
            #There might be some errors in encodings
            print "Book: "+self.fileName+" "+str(xcpt)

    #Search for a word in book. Time complexity O(1)
    def findWord(self,word):
        if word in self.wordDict:
            return (self.fileName,self.wordDict[word])
        else:
            return None