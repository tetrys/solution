#Class that holds the info
#For every word in the word Dictionary

class WordInfo:

    def __init__(self,occur=0,books=[]):
        #Number of total occurences in books
        self.occurences = occur
        #The next word that has the most occurences
        self.nextWord = ""
        #List of tuples book name (string) number of occurences of word(int)
        self.bookList = books
        #Flag to sort the book list of occurences if needed
        self.bookListSorted = False
