import io
import sys
import os
import string
import shutil
from shutil import copyfile
import argparse
from search_books import book_collection
import time

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Copy book files")
    parser.add_argument("--copies",metavar="N", type=int,default="0",required=False)
    parser.add_argument("--init",action="store_true",default=False)
    parser.add_argument("--save",action="store_true",default=False)
    parser.add_argument("--load",action="store_true",default=False)
    parser.add_argument("--validate",metavar="M", type=int,default="0",required=False)
    parser.add_argument("--iter",action="store_true",default=False)
    parser.add_argument("--update",action="store_true",default=False)
    parser.add_argument("--test",action="store_true",default=False)

    args = parser.parse_args()

    copiesNum = args.copies

    validoccurence = args.validate

    mainPath = os.path.dirname(os.path.realpath(__file__))
    booksPath = os.path.join(mainPath,"books")
    booksTarPath = os.path.join(mainPath,"books.tar.gz")

    extractBooks = book_collection.extractBooks
    initializeFunction = book_collection.initializeFunction
    printCommon = book_collection.printCommon
    findWord = book_collection.findWord
    saveBooks = book_collection.saveBooks
    loadBooks = book_collection.loadBooks

    dbFile1Path = os.path.join(mainPath,"mostCommonWord.bdb")
    dbFile2Path = os.path.join(mainPath,"wordList.bdb")

    #print mainPath
    #print booksTarPath
    #print booksPath

    updateDB = args.update

    iterateCopies = args.iter

    stVal = 0
    if not iterateCopies:
        stVal = copiesNum-1

    for i in xrange(stVal,copiesNum):
        if os.path.isdir(booksPath):
            shutil.rmtree(booksPath)
    
        if copiesNum>1 or updateDB:
            if os.path.isfile(dbFile1Path):
                print "Removing "+dbFile1Path
                os.remove(dbFile1Path)
            if os.path.isfile(dbFile2Path):
                print "Removing "+dbFile2Path
                os.remove(dbFile2Path)

        print "Books in Tar: "+booksTarPath

        extractBooks(booksTarPath)

        if(os.path.isdir(booksPath)):
            print "Creating: "+str(i+1)+" copies."
            for bookFile in os.listdir(booksPath):
                for j in xrange(i):
                    copyfile(os.path.join(booksPath,bookFile),os.path.join(booksPath,"C_"+str(j)+bookFile))
        
        initData = args.init

        if initData:
            
            print "Default Buffer: "+str(io.DEFAULT_BUFFER_SIZE)

            initTimes = []

            startTime = time.time()
            initializeFunction(booksTarPath)
            endTime = time.time()

            initTimes.append(endTime-startTime)

            if iterateCopies:
                print "Loop with "+str(i+1)+" copies -> Inited in: "+str(initTimes[i])+" sec -> Ratio: "+str(initTimes[i]/initTimes[0])
            else:
                print "Loop with "+str(i+1)+" copies -> Inited in: "+str(initTimes[0])+" sec"

            saveData = args.save

            if saveData:
                print "Saving to Binary"
                startTime = time.time()
                saveBooks(mainPath)
                saveTime = time.time() - startTime
                print "Save Time: "+str(saveTime)+" sec"

    loadData = args.load

    if loadData and validoccurence==0:
        print "Loading from Binary"
        startTime = time.time()
        loadBooks(mainPath)
        loadTime = time.time() - startTime
        
        startTimeCommon = time.time()
        result = printCommon("100")
        commonTime = time.time() - startTimeCommon
        print("\n".join(result))
        
        startTimeSearch = time.time()
        result = findWord("der")
        searchTime = time.time() - startTimeSearch

        if len(result)<=0:
            result.append("")
        print("\n".join(result))
        
        print "Load Time: "+str(loadTime)+" sec"
        print "Common Time: "+str(commonTime)+" sec"
        print "Search Time: "+str(searchTime)+" sec"

    elif (validoccurence > 0):
        print "Loading from Binary"
        startTime = time.time()
        loadBooks(mainPath)
        loadTime = time.time() - startTime
        
        startTimeCommon = time.time()
        result = printCommon(str(validoccurence))
        commonTime = time.time() - startTimeCommon

        if len(result)<=0:
            result.append("")

        print("\n".join(result))

        searchWord = result[validoccurence-1].split()[0]
        
        wrdOcr = -1
        try:
            wrdOcr = int(result[validoccurence-1].split()[1])
        except:
            pass

        print "Searching for: "+searchWord

        startTimeSearch = time.time()
        result = findWord(searchWord)
        searchTime = time.time() - startTimeSearch
        print("\n".join(result))

        print "Load Time: "+str(loadTime)+" sec"
        print "Common Time: "+str(commonTime)+" sec"
        print "Search Time: "+str(searchTime)+" sec"

        ocr = 0
        for rs in result:
            try:
                ocr+=int(rs.split()[1])
            except:
                pass
        
        print "Accumulated occurences: "+str(ocr)
        if(ocr==wrdOcr):
            print "Test Common PASS"
        else:
            print "Test Common FAIL"

    doTesting = args.test

    if doTesting:
        print "Testing"
        
        print "Testing empty"
        result = findWord("")
        if len(result)>1 or result[0]!="Word not found!":
            print "Empty Test FAIL"
        else:
            print "Empty Test PASS"

        print "Testing space"
        result = findWord(" ")
        if len(result)>1 or result[0]!="Word not found!":
            print "Space Test FAIL"
        else:
            print "Space Test PASS"

        print "Testing der"
        result = findWord("der")
        if not result:
            print "Space Test FAIL"
        else:
            print("\n".join(result))
            print "Space Test PASS"