# -*- coding: utf-8 -*-
import sys
import os
import SocketServer
from search_books.book import Book
from search_books import book_collection

class MyTCPHandler(SocketServer.StreamRequestHandler):

    def handle(self):
        _handlers = {'common': self.common_cmd,
                     'search': self.search_cmd}
        data = self.rfile.readline().strip()
        l = data.split()
        if len(l) >= 2:
            command, args = l[0], l[1:]
            f = _handlers.get(command, None)
            if f:
                ans = f(*args)
            else:
                ans = 'Invalid command'
        else:
            ans = 'Invalid Usage'
        self.request.sendall(ans + '\r\n')

    def common_cmd(self, *args):
        """Add your code here for the common command
        This function should return a string with the
        most n most common words in the books
        """
        ans = []
        
        #The server could start populating the data structure when a user makes the first request
        #Check the server status
        status = book_collection.initialize(os.path.dirname(os.path.realpath(__file__))+"/books.tar.gz")

        if (status==1):
            #The server just started creating the data structure
            ans.append("Server is Starting. Please try in a moment...")
        elif (status==0):
            #The server can respond
            try:
                commonNum = int(*args)
                ans = book_collection.printCommon(commonNum)
            except:
                ans = ["Invalid Input. Integer Expected."]
        elif (status==2):
            #The server is still busy creating the data structure
            ans.append("Server is Busy. Please try in a moment...")
                    
        return '\n'.join(ans)

    def search_cmd(self, *args):
        """Add your code here for the search command
        Should return a a string with the documents the
        word appears into"""
        ans = []
        
        #The server could start populating the data structure when a user makes the first request
        #Check the server status
        status = book_collection.initialize(os.path.dirname(os.path.realpath(__file__))+"/books.tar.gz")

        if(status==1):
            #The server just started creating the data structure
            ans.append("Server is Starting. Please try in a moment...")
        elif(status==0):
            #The server can respond
            ans = book_collection.findWord(*args)
        elif(status==2):
            #The server is still busy creating the data structure
            ans.append("Server is Busy. Please try in a moment...")

        return '\n'.join(ans)


if __name__ == "__main__":
    HOST, PORT = "0.0.0.0", 9999

    #We start creating the data structure here
    #Start creating the data structures
    book_collection.initialize(os.path.dirname(os.path.realpath(__file__))+"/books.tar.gz")
    
    server = SocketServer.TCPServer((HOST, PORT), MyTCPHandler)
    server.serve_forever()